// contains functions and business logic of our express js application
// all operations it can do will be placed in this file
// allows us to use the contents of "task.js" file in the models folder
const Task = require("../models/task.js");

// defines the functions to be used in the "taskRoutes.js"
module.exports.getAllTasks = () =>{
	return Task.find({}).then(result =>{
		return result;
	});
};


// function to create a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});
	//waits for the save method to complete before detecting any errors
	return newTask.save().then((task,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	});
};



/*
	MINIACTIVITY
		- function to delete a task
			- look for the task using "findByIdandRemove" with the corresponding id provided/passed from the URI/route

			- delete the task
				- wait for the deletion and then if there are errors, print the error in console
				- if no errors, return the removed task
*/

module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - is a mongoose method that searches for the documents using _id properties
	// after finding, the job of this method is to remove the document.
	return Task.findByIdAndRemove(taskId).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return result;
		};
	});
};



// function to update a task
/*
	BUSINESS Model
		- find the task by using the mongoose method "findById"
		- replace the task's name returned from the database with the "name" property from the request body
		- save the task

*/
module.exports.updateTask = (taskId,requestBody) => {
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.name = requestBody.name;
			return result.save().then((updatedTask, error)=>{
				if(error){
					console.log(error);
					return false;
				}
				else{
					return updatedTask;
				};
			});
		};
	});
};



// function for retrieving a specific task

module.exports.findTask = (taskId) => {
	return Task.findById(taskId).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return result;
		};
	});
};


module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.status = "complete";
			return result.save().then((completedTask,error)=>{
				if(error){
					console.log(error);
					return false;
				}
				else{
					return completedTask;
				};
			});
		};
	});
};
