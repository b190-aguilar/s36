// contains all endpoints for our application
// apps.js/index.js should only be concerned with information about the server
// we have to use the Router method inside express

const express = require("express");
// allows us to create or access HTTP method middlewares that makes it easier to create routing systems.	
const router = express.Router();

// this allows us to use the contents of "taskController.js" in "controllers" folder
const taskController = require("../controllers/taskController.js")


// route to get all tasks
/*
	get request	 to be sent at localHost:3000/tasks
*/
router.get("/", (req,res)=>{
	// since the controller did not send any response, the task falls to the routes to send a response that is received from the controllers and send it to the frontend.
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});


/*
	MINIACTIVITY
		create "/" route that will process the request and send the response based on the createTasks fxn inside the "taskController.js"
*/

router.post("/", (req,res)=>{
	console.log(req.body);
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});



// route for deleting a task
router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});




// route for updating a task
router.put("/:id", (req,res) =>{
	/*
		req.params.id retrieves the taskId of the document to be updated
		req.body determines what updates are to be done in the document coming from the request body
	*/
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});





// ACTIVITY


// route for getting a specific task
router.get("/:id", (req,res) =>{
	taskController.findTask(req.params.id).then(resultFromController => res.send(resultFromController));
});


// route for changing status to complete
router.put("/:id/complete", (req,res) =>{
	taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
});


// module.exports allows us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports = router;

